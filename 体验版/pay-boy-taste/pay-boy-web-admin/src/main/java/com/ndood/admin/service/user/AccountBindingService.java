package com.ndood.admin.service.user;

import com.ndood.admin.core.exception.AdminException;

public interface AccountBindingService {

	/**
	 * 判断手机号是否绑定了其它账号
	 */
	boolean isMobileBindingAccounts(String mobile);
	/**
	 * 绑定新手机
	 */
	void bindingMobile(String userId, String mobile) throws AdminException;
	/**
	 * 判断邮箱是否绑定了其它账号
	 */
	boolean isEmailBindingAccounts(String email);
	/**
	 * 绑定新邮箱
	 */
	void bindingEmail(String userId, String email) throws AdminException;

}
