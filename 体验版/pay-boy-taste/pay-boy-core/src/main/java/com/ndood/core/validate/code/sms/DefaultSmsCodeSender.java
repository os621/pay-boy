package com.ndood.core.validate.code.sms;
/**
 * 模拟短信验证码发送类
 * @author ndood
 */
public class DefaultSmsCodeSender implements SmsCodeSender{

	/**
	 * 模拟发送短信
	 */
	@Override
	public void send(String mobile, String code) {
		System.out.println("向手机"+mobile+"发送短信验证码"+code);
	}
	
}