package com.ndood.admin.controller.comm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ndood.admin.core.properties.AdminProperties;
import com.ndood.admin.core.web.tools.FileUploadUtils;
import com.ndood.admin.pojo.comm.dto.FileInfo;
import com.ndood.admin.pojo.comm.vo.AdminResultVo;

@Controller
public class ImageUploadController {
	private String folder = "E:\\workspaces\\sts\\pay-boy\\pay-boy-web-admin\\src\\main\\java\\com\\ndood\\web\\admin\\web\\controller";
	
	@Autowired
	private FileUploadUtils fileUploadUtils;
	
	@Autowired
	private AdminProperties adminProperties;
	
	/**
	 * 上传多个文件
	 */
	@PostMapping("/oss/aliyun/upload")
	@ResponseBody
	public Object uploadFiles(HttpServletRequest request) throws Exception {
		
		// Step1: 对请求参数进行校验
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		if (fileMap == null || fileMap.size() == 0) {
			throw new Exception("文件内容为空!!!");
		}

		// Step2: 上传文件
		Collection<MultipartFile> files = fileMap.values();
		List<String> urls = new ArrayList<String>();
		for (MultipartFile file : files) {
			String url = fileUploadUtils.upload(adminProperties.getOss().getBucketName(), adminProperties.getOss().getDir(), file);
			urls.add(url);
		}

		// Step3: 返回结果
		return AdminResultVo.ok().setMsg("上传成功").setData(StringUtils.join(urls, ','));
	
	}
	
	/**
	 * 上传文件controller
	 * @param file
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/oss/aliyun/upload")
	@ResponseBody
	public FileInfo upload(MultipartFile file) throws Exception{
		
		System.out.println(file.getName());
		System.out.println(file.getOriginalFilename());
		System.out.println(file.getSize());
		
		File localFile = new File(folder, new Date().getTime()+".txt");
		file.transferTo(localFile);
		
		return new FileInfo(localFile.getAbsolutePath());
	
	}
	
	/**
	 * 下载文件controller
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@GetMapping("/oss/aliyun/download")
	public void download(String id,HttpServletRequest request, HttpServletResponse response) throws Exception{
		
		// jdk7的写法
		/*try {
			InputStream inputStream = new FileInputStream(new File(folder,id+".txt"));
			OutputStream outputStream = response.getOutputStream();
		} catch (Exception e) {
		}*/
		try(InputStream inputStream = new FileInputStream(new File(folder,id+".txt"));
				OutputStream outputStream = response.getOutputStream();) {
			response.setContentType("application/x-download");
			response.addHeader("Content-Disposition","attachment;filename=test.txt");
			IOUtils.copy(inputStream, outputStream);
			outputStream.flush();
		}
		
	}
}
