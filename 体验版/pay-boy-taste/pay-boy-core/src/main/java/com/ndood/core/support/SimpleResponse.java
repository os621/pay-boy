package com.ndood.core.support;

/**
 * 自定义简单返回类
 * @author ndood
 */
public class SimpleResponse {

	private String code;
	private Object content;
	
	public SimpleResponse(String code, Object content) {
		super();
		this.code = code;
		this.content = content;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getContent() {
		return content;
	}

	public void setContent(Object content) {
		this.content = content;
	}
	
}