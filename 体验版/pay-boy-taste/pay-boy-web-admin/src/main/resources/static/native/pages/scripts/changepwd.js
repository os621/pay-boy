define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'bootstrap',
	'layui'
], function($) {
	return {
		init: function() {
			
			//1 表单校验
			$('#change_form').validate({
	            errorElement: 'span', // 默认错误显示位置
	            errorClass: 'help-block', // 默认错误显示样式
	            focusInvalid: true, // 获得焦点时是否验证
	            // 验证规则
	            rules: {
	            	password:{
	            		required: true
	            	},
	            	repassword:{
	            		required: true,
	            		equalTo: '#pwd'
	            	}
	            },
	            // 验证提示
	            messages: {
	            	username:{
	            		required: '密码不能为空！'
	            	},
	            	repassword:{
	            		required: '密码不能为空！',
	            		equalTo: '两次输入的密码不一致！'
	            	},
	            },
	            // 只要不成功，就显示.alert-dander div
	            invalidHandler: function(event, validator) {
	            	$('.alert', $('#change_form')).hide();
	                $('.alert-danger', $('#change_form')).show();
	            },
	            // 错误输入框高亮显示
	            highlight: function(element) { 
	                $(element).closest('.form-group').removeClass('valid').addClass('has-error'); 
	            },
	            // 验证成功，移除错误样式
	            success: function(label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },
	            // 自定义错误提示位置
	            errorPlacement: function(error, element) {
	            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
	            	if(error.text()!=''){
	            		console.log(error.text());
	            		$(errorDom).text(error.text());
	            	}
	            },
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
	        });
			
			// 2 监听回车事件
	        $('#change_form input').keypress(function(e) {
	            if (e.which == 13) {
	                if ($('#change_form').validate().form()) {
	                	$('#change_submit').click();
	                }
	                return false;
	            }
	        });
	        
	        // 3 提交修改密码请求
	        $('#change_submit').click(function(){
	        	// Step1: form表单校验，远程校验用户名是否被占用。密码字段根据username判断，如果是邮箱注册则需要进行校验
	        	var valid = $('#change_form').valid();
	        	$('#change_form').find('input[name="password"]').valid();
	        	$('#change_form').find('input[name="repassword"]').valid();
	        	if(!valid){
	        		return;
	        	}
	        	
	        	$('.alert-danger', $('#change_form')).val('').hide();
	        	var data =  $('#change_form').serializeJson();
	        	$('#change_submit').button('loading')
	        	$.ajax({
	            	url: '/user/forget/email_change_pwd',
	                type:'post',
	                dataType:'json',
	                contentType:'application/x-www-form-urlencoded',
	                data: data,
	                async:true,
	                success:function(data){
	                	$('#change_submit').button('reset')
	                	// 处理注册失败
	                	if(data.code!='10000'){
	                		$('.alert', $('#change_form')).hide();
                    		$('.alert-danger', $('#change_form')).find('span').html(data.msg);
                    		$('.alert-danger', $('#change_form')).show(); 
                    		return false;
                    	}
	                	
	                	var email = $("#change_form").find('input[name="email"]').val();
	                	window.location.href = '/user/forget/to_success?type=email&username='+email;
	                	return false;
	                },
	                // 自定义错误提示位置
		            errorPlacement: function(error, element) {
		            	var errorDom = $(element).parents('form').find('.alert-danger:eq(0)').find('span:eq(0)');
		            	if(error.text()!=''){
		            		console.log(error.text());
		            		$(errorDom).text(error.text());
		            	}
		            }
	            });
	        });	
		}
	}
});