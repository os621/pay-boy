package com.ndood.admin.core.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

import com.ndood.core.authorize.AuthorizeConfigProvider;
import com.ndood.core.properties.SecurityProperties;

/**
 * Admin模块相关的访问权限配置 只有demo模块才知道有这个配置；AuthorizeConfigManager会把所有的provider都装载起来
 * 确保rbac配置在最后生效
 */
@Component
@Order(Integer.MAX_VALUE)
public class AdminAuthorizeConfigProvider implements AuthorizeConfigProvider{

	@Autowired
	private SecurityProperties securityProperties;
	
	/**
	 * demo 模块的专属权限配置
	 */
	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		
		// druid貌似不用配置就可以访问
		config
		.antMatchers("/static/**","/favicon**").permitAll()
		.antMatchers("/error/**").permitAll()
		.antMatchers("/user/check_**").permitAll()
		.antMatchers("/user/to_**").permitAll()
		.antMatchers("/user/**_regist").permitAll()
		.antMatchers("/user/regist/**").permitAll()
		.antMatchers("/user/**_login").permitAll()
		.antMatchers("/user/forget/**").permitAll()
		.antMatchers("/connect/**").permitAll()
		.antMatchers("/connect**").permitAll()
		
		.antMatchers(securityProperties.getBrowser().getSignInPage()).permitAll()
		.antMatchers(securityProperties.getBrowser().getSignUpPage()).permitAll()
		.antMatchers(securityProperties.getBrowser().getSignOutUrl()).permitAll()
		
		.antMatchers("/star/starinfo/validate_name").permitAll()
		.antMatchers("/star/starrank/period").permitAll()
		
		.antMatchers("/").authenticated()
		.antMatchers("/index").authenticated()
		.antMatchers("/center").authenticated()
		.antMatchers("/jump_**").authenticated()
		.antMatchers("/**/**/query").authenticated()
		.antMatchers("/**/**/treeview").authenticated()
		.antMatchers("/user/account/binding/**").authenticated()
		.antMatchers("/system/department/department_tree").authenticated()
		.antMatchers("/system/region/province").authenticated()
		.antMatchers("/oss/aliyun/upload").authenticated();
		
		/*.antMatchers("/user").hasRole("ADMIN")
		//.antMatchers(HttpMethod.GET,"/user/*").hasRole("ADMIN")
		//.antMatchers(HttpMethod.GET,"/user/*").hasAuthority("user")
		.antMatchers(HttpMethod.GET,"/user/*").access("xxxxxx")
		
		// 网页授权
		.antMatchers("/demo.html").hasRole("TEST");*/
		
		// 自定义授权表达式，处理rbac请求
		config.anyRequest().access("@rbacPermissionHandler.hasPermission(request, authentication)");

		//.anyRequest().authenticated();
	}

}