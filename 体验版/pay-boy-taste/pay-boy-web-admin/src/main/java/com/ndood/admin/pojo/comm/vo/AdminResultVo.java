package com.ndood.admin.pojo.comm.vo;

import java.io.Serializable;

import com.alibaba.fastjson.annotation.JSONField;
import com.ndood.admin.core.constaints.AdminErrCode;

/**
 * 普通对象返回数据
 */
public class AdminResultVo implements Serializable {
	private static final long serialVersionUID = 3765308055181715685L;

	public final static AdminResultVo ok() {
		return new AdminResultVo(AdminErrCode.SUCCESS);
	}
	
	public final static AdminResultVo error() {
		return new AdminResultVo(AdminErrCode.ERR_OTHER);
	}
	
	private AdminErrCode code;
	private String msg;
	private Object data;
	
	private AdminResultVo(AdminErrCode code) {
		this.code = code;
		this.msg = code.getValue();
	}

	@JSONField(name = "code")
	public String getCode() {
		return code.getCode();
	}
	
	@JSONField(name = "code")
	public AdminResultVo setCode(String code) {
		this.code = AdminErrCode.getEnum(code);
		return this;
	}

	public String getMsg() {
		return msg;
	}
	
	public AdminResultVo setMsg(String msg) {
		this.msg = msg;
		return this;
	}

	public Object getData() {
		return data;
	}

	public AdminResultVo setData(Object data) {
		this.data = data;
		return this;
	}
}
