package com.ndood.admin.repository.system;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.DepartmentPo;

/**
 * 部门dao
 * @author ndood
 */
public interface DepartmentRepository extends JpaRepository<DepartmentPo, Integer> {

	List<DepartmentPo> findByOrderBySort();

}
